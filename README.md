# Problème du sac à dos

## Énoncé

L’énoncé de ce problème est simple : « Étant donné plusieurs objets possédant chacun un poids et une valeur et étant donné un poids maximum pour le sac, quels objets faut-il mettre dans le sac de manière à maximiser la valeur totale sans dépasser le poids maximal autorisé pour le sac ? ».

## Ressources

Ce site a pour objectif de visualiser et expérimenter un algorithme de résolution du problème.
