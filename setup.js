const objets = [
    { "nom" : "Argenterie", "image" : "assets/argenterie.png", "poids" : 13, "valeur" : 700},
    { "nom" : "Bijou", "image" : "assets/bijou.jpg", "poids" : 12, "valeur" : 400},
    { "nom" : "Chandelier", "image" : "assets/chandelier.png", "poids" : 8, "valeur" : 300},
    { "nom" : "Drageoir", "image" : "assets/drageoir.jpeg", "poids" : 10, "valeur" : 300},
    { "nom" : "Encrier", "image" : "assets/encrier.jpeg", "poids" : 6, "valeur" : 350},
    { "nom" : "Faience", "image" : "assets/faience.png", "poids" : 9, "valeur" : 425},
    { "nom" : "Guéridon", "image" : "assets/gueridon.png", "poids" : 7, "valeur" : 250},
    { "nom" : "Horloge", "image" : "assets/horloge.png", "poids" : 12, "valeur" : 750},
    { "nom" : "Imprimante", "image" : "assets/imprimante.png", "poids" : 1, "valeur" : 150},
    { "nom" : "Jardinière", "image" : "assets/jardiniere.jpeg", "poids" : 13, "valeur" : 900}
];

function sanitize(parameter){
  if (parameter == null){
    return 4;
  }
  let n = parseInt(parameter);

  return (isNaN(n))?4:((n <= 4)?4:((n>objets.length)?objets.length-1:n));
}

const urlParams = new URLSearchParams(window.location.search);
const n = sanitize(urlParams.get('n'));

const capacite = 30;
